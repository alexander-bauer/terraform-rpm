.PHONY: rpm sources

rpm: terraform.spec sources
	rpmbuild -bb terraform.spec --define "_topdir $(PWD)"

sources: terraform.spec
	mkdir -p SOURCES/
	spectool -g -C "SOURCES/" terraform.spec
